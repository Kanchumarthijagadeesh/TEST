package jdbc;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

public class Encryption_and_Decryption {
 public static void main(String[] args) {

	Encoder enconder=Base64.getEncoder();
	String origenalValue="jagadee157";
	String enconderString=enconder.encodeToString(origenalValue.getBytes());
	System.out.println("Encrapted value :"+enconderString);
	
	Decoder decoder=Base64.getDecoder();
	byte[] bytes=decoder.decode(enconderString);
	System.out.println("Decryption :"+new String(bytes));
 }
}
